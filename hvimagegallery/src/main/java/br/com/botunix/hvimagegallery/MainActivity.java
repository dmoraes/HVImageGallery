package br.com.botunix.hvimagegallery;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import br.com.botunix.singleselectablepicture.SingleSelectablePicture;

import static br.com.botunix.singleselectablepicture.SingleSelectablePicture.MY_REQUEST_CODE;

public class MainActivity extends AppCompatActivity {

    HVImageGallery imageGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageGallery = findViewById(R.id.hvimagegallery);
        askForPermission();
    }

    private void askForPermission() {
        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        this.requestPermissions(permissions, MY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        imageGallery.onActivityResult(requestCode, resultCode, imageReturnedIntent);
    }
}
