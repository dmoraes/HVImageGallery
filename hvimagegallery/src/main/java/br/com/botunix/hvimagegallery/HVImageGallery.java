package br.com.botunix.hvimagegallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.provider.MediaStore;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import br.com.botunix.singleselectablepicture.SingleSelectablePicture;

/**
 * Created by venturus on 18/01/18.
 */

public class HVImageGallery extends ConstraintLayout implements SingleSelectablePicture.ActivityResultInterface  {

    private Context mContext;
    private Button buttonAdd;
    private PopupMenu popup;
    private HorizontalScrollView scrollView;
    private LinkedList<SingleSelectablePicture> pictures = new LinkedList<>();
    private LinearLayout linearLayoutSSPContainer;
    private HashMap<Integer, SingleSelectablePicture> activityMap;
    private TextView clickTextView;
    private FloatingActionButton addRemoveAction;
    private Boolean isSelecting = false;

    private ArrayList<SingleSelectablePicture> images;

    public HVImageGallery(Context context) {
        super(context);
        initializeViews(context);
    }

    public HVImageGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public HVImageGallery(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    public void add() {

    }

    private void initializeViews(Context context) {
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.hvimagegallery, this);

        linearLayoutSSPContainer = findViewById(R.id.linearLayoutSSPContainer);
        activityMap = new HashMap<>();
        scrollView = findViewById(R.id.horizontalScrollView);
        scrollView.setOnScrollChangeListener(new OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                if(activityMap!=null) {
                    for(SingleSelectablePicture mssp : activityMap.values()) {
                        Rect scrollBounds = new Rect();
                        scrollView.getHitRect(scrollBounds);
                        if (mssp.getLocalVisibleRect(scrollBounds)) {
                            // Any portion of the imageView, even a single pixel, is within the visible window
                            Log.i("=====Botunix===> ", "Loading ssp: " + mssp.hashCode());
                        } else {
                            // NONE of the imageView is within the visible window
                            Log.i("=====Botunix===> ", "Unloading ssp: " + mssp.hashCode());
                        }
                    }
                }
            }
        });
        addRemoveAction = findViewById(R.id.addRemoveAction);
        addRemoveAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isSelecting) {
                    popup = new PopupMenu(getContext(), view);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getItemId() == br.com.botunix.singleselectablepicture.R.id.camera) {
                                //takePicture();
                                addCameraPicture();
                                return true;
                            } else if (item.getItemId() == br.com.botunix.singleselectablepicture.R.id.gallery) {
                                //selectPicture();
                                addGalleryPicture();
                                return true;
                            }
                            return false;
                        }
                    });
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(br.com.botunix.singleselectablepicture.R.menu.picture_source, popup.getMenu());
                    popup.show();
                } else {
                    for(int i=(pictures.size()-1); i>=0; i--) {
                        SingleSelectablePicture mssp = pictures.get(i);
                        if(mssp.getSelected().isChecked()) {
                            linearLayoutSSPContainer.removeView(mssp);
                            pictures.remove(i);
                        }
                    }
                    checkIsSelecting();
                }
            }
        });
    }

    private void addGalleryPicture() {
        SingleSelectablePicture ssp = new SingleSelectablePicture(getContext(), this);
        ssp.manualInflate();
        linearLayoutSSPContainer.addView(ssp);
        ssp.setLayoutParams(new LinearLayout.LayoutParams(400, ViewGroup.LayoutParams.MATCH_PARENT));
        pictures.add(ssp);
        ssp.selectPicture();
    }

    private void addCameraPicture() {
        SingleSelectablePicture ssp = new SingleSelectablePicture(getContext(), this);
        ssp.manualInflate();
        linearLayoutSSPContainer.addView(ssp);
        ssp.setLayoutParams(new LinearLayout.LayoutParams(400, ViewGroup.LayoutParams.MATCH_PARENT));
        pictures.add(ssp);
        ssp.takePicture();
    }

    private void addEmptySSP() {
        SingleSelectablePicture ssp = new SingleSelectablePicture(getContext(), this);
        ssp.manualInflate();
        linearLayoutSSPContainer.addView(ssp);
        ssp.setLayoutParams(new LinearLayout.LayoutParams(400, ViewGroup.LayoutParams.MATCH_PARENT));
        pictures.add(ssp);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        Log.e("===== Botunix ====> ", requestCode + " -- " + resultCode);
        SingleSelectablePicture currentSSP = activityMap.get(requestCode);
        if(currentSSP != null) {
                currentSSP.onActivityResult(resultCode, imageReturnedIntent);
        }
    }

    @Override
    public void setActivityForResult(Integer requestCode, SingleSelectablePicture singleSelectablePicture) {
        activityMap.put(requestCode, singleSelectablePicture);
    }

    @Override
    public void checkIsSelecting() {
        for(SingleSelectablePicture ssp : pictures) {
            if(ssp.getSelected().isChecked()) {
                this.isSelecting = true;
                addRemoveAction.setImageResource(R.drawable.ic_delete_black_24dp);
                addRemoveAction.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext,R.color.colorAccent)));
                //ContextCompat.getColor(mContext,R.color.colorAccent)
                return;
            }
        }
        addRemoveAction.setImageResource(R.drawable.ic_add_black_24dp);
        addRemoveAction.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext,R.color.colorPrimary)));
        this.isSelecting = false;
    }

}
