package br.com.botunix.singleselectablepicture;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.FileProvider;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.PopupMenu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by venturus on 08/01/18.
 */

public class SingleSelectablePicture extends ConstraintLayout {

    private ConstraintLayout merge;
    private ImageButton image;
    private CheckBox selected;
    private PopupMenu popup;
    private File photoFile = null;
    private Uri returnedPhoto = null;
    private Context mContext;
    private String[] permissions;
    private int[] grantResults;
    private ActivityResultInterface host;
    public static final Integer MY_REQUEST_CODE = 0;
    public static final Integer TAKE_PICTURE = 0;
    public static final Integer SELECT_PICTURE = 1;
    private Integer currentGetPictureType;

    public ActivityResultInterface getHost() {
        return host;
    }

    public void setHost(ActivityResultInterface host) {
        this.host = host;
    }

    public Integer getCurrentGetPictureType() {
        return currentGetPictureType;
    }

    public void setCurrentGetPictureType(Integer currentGetPictureType) {
        this.currentGetPictureType = currentGetPictureType;
    }

    public CheckBox getSelected() {
        return selected;
    }

    public void setSelected(CheckBox selected) {
        this.selected = selected;
    }


    public interface ActivityResultInterface {
        void setActivityForResult(Integer requestCode, SingleSelectablePicture singleSelectablePicture);
        void checkIsSelecting();
    }

    public SingleSelectablePicture(Context context) {
        super(context);
        initializeViews(context);
    }

    public SingleSelectablePicture(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public SingleSelectablePicture(Context context, ActivityResultInterface host) {
        super(context);
        this.host = host;
        initializeViews(context);
    }

    public SingleSelectablePicture(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        mContext = context;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.single_selectable_picture, this);
    }

    public void manualInflate() {
        this.onFinishInflate();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        image = (ImageButton)this.findViewById(R.id.sspImageButton);
        merge = (ConstraintLayout)this.findViewById(R.id.merge);
        setSelected((CheckBox)this.findViewById(R.id.checkBox));
        this.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageOptions(v);
            }
        });
        this.image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //already selected
                if(getSelected().isChecked()) {
                    image.setAlpha(1.0f);
                    getSelected().setChecked(false);
                } else { //not selected yet
                    image.setAlpha(0.5f);
                    getSelected().setChecked(true);
                }
                host.checkIsSelecting();
                return true;
            }
        });
        this.getSelected().setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!b) {
                    image.setAlpha(1.0f);
                } else { //not selected yet
                    image.setAlpha(0.5f);
                }
                host.checkIsSelecting();
            }
        });
    }

    public void showImageOptions(View view) {
        if(!getSelected().isChecked()) {
            popup = new PopupMenu(getContext(), view);
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.camera) {
                        takePicture();
                        return true;
                    } else if (item.getItemId() == R.id.gallery) {
                        selectPicture();
                        return true;
                    }
                    return false;
                }
            });
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.picture_source, popup.getMenu());
            popup.show();
        }
    }

    public void takePicture() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                // Create the File where the photo should go
                photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                            "br.com.botunix.singleselectablepicture.fileprovider",
                            photoFile);
                    returnedPhoto = photoURI;
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    if (getContext() instanceof Activity) {
                        Integer intentHashCode = 101;
                        this.setCurrentGetPictureType(TAKE_PICTURE);
                        host.setActivityForResult(intentHashCode, this);
                        ((Activity) getContext()).startActivityForResult(takePictureIntent, intentHashCode);
                    }
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public void selectPicture() {
        Intent selectPicture = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        int intentHashCode = 100;
        this.setCurrentGetPictureType(SELECT_PICTURE);
        host.setActivityForResult(intentHashCode, this);
        ((Activity) getContext()).startActivityForResult(selectPicture, intentHashCode);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        switch(requestCode) {
            case 0:
                if(resultCode == Activity.RESULT_OK){
                    galleryAddPic(photoFile.getAbsolutePath());
                    this.image.setImageURI(returnedPhoto);
                    this.image.setTag(returnedPhoto);
                }

                break;
            case 1:
                if(resultCode == Activity.RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    this.image.setImageURI(selectedImage);
                    this.image.setTag(selectedImage);
                }
                break;
        }
    }

    public void onActivityResult(int resultCode, Intent imageReturnedIntent) {
        switch(currentGetPictureType) {
            case 0:
                if(resultCode == Activity.RESULT_OK){
                    galleryAddPic(photoFile.getAbsolutePath());
                    try {
                        InputStream input = this.getContext().getContentResolver().openInputStream(returnedPhoto);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = false;
                        options.inSampleSize = 4;
                        Bitmap bitmap = BitmapFactory.decodeStream(input, this.image.getClipBounds(), options);
                        this.image.setImageBitmap(bitmap);
                        this.image.setTag(returnedPhoto);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case 1:
                if(resultCode == Activity.RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        InputStream input = this.getContext().getContentResolver().openInputStream(selectedImage);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = false;
                        options.inSampleSize = 4;
                        Bitmap bitmap = BitmapFactory.decodeStream(input, this.image.getClipBounds(), options);
                        this.image.setImageBitmap(bitmap);
                        this.image.setTag(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        this.permissions = permissions;
        this.grantResults = grantResults;
    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        String path = ((Activity) getContext()).getExternalMediaDirs()[0].getAbsolutePath() + "/Pictures/";
        //String path = "/storage/emulated/0/Android/media/br.com.botunix.singleselectablepicture/Pictures/";
        File storageDir = new File(path);
        if(!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic(String file) {
        MediaScannerConnection.scanFile(this.getContext(),
                new String[] { file }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.d("Botunix ===> ", path);
                    }
                });
    }
}
