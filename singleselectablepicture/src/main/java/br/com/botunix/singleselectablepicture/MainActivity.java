package br.com.botunix.singleselectablepicture;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import static br.com.botunix.singleselectablepicture.SingleSelectablePicture.MY_REQUEST_CODE;

public class MainActivity extends AppCompatActivity {

    SingleSelectablePicture ssp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ssp = findViewById(R.id.ssp);
        askForPermission();
    }

    private void askForPermission() {
        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        this.requestPermissions(permissions, MY_REQUEST_CODE);
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e("===== Botunix ===>", permissions.toString());
        ssp.onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        ssp.onActivityResult(requestCode, resultCode, imageReturnedIntent);
    }

}
